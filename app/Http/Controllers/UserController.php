<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = [
        //     ['nama' => 'Fatur', 'alamat' => 'Yogyakarta', 'asal-univ' => 'UPN'],
        //     ['nama' => 'Ryzal', 'alamat' => 'Sumedang', 'asal-univ' => 'UPN'],
        //     ['nama' => 'Samuel', 'alamat' => 'Purworejo', 'asal-univ' => 'UKDW'],
        //     ['nama' => 'Edwin', 'alamat' => 'Yogyakarta', 'asal-univ' => 'UKDW'],
        //     ['nama' => 'Kiki', 'alamat' => 'Yogyakarta', 'asal-univ' => 'UKDW'],
        //     ['nama' => 'Nuel', 'alamat' => 'Tegal', 'asal-univ' => 'UKDW'],
        //     ['nama' => 'Novianto', 'alamat' => 'Sumatera selatang', 'asal-univ' => 'Atma'],
        //     ['nama' => 'Bagas', 'alamat' => 'Kalimantan', 'asal-univ' => 'Atma'],
        //     ['nama' => 'Christo', 'alamat' => 'Tegal', 'asal-univ' => 'Atma'],
        //     ['nama' => 'Evan', 'alamat' => 'Solo', 'asal-univ' => 'Atma'],
        // ];
        $data = Member::all();
        return view('table/index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);
        // return view('user/show', ['member' => $member]);
        return view('user/show', ['member' => $member, 'edit' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        // return view('user/edit', ['member' => $member]);
        return view('user/show', ['member' => $member, 'edit' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updateMember = Member::find($request->id);
        $updateMember->name = $request->name;
        $updateMember->asal = $request->asal;
        $updateMember->university = $request->university;
        // $updateMember->save();
        
        if( $updateMember->save()) {
            return redirect('/user');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::destroy($id);
        return redirect('/user');
    }
}
