<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Member;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            ['name' => 'Fatur', 'asal' => 'Yogyakarta', 'university' => 'UPN'],
            ['name' => 'Ryzal', 'asal' => 'Sumedang', 'university' => 'UPN'],
            ['name' => 'Samuel', 'asal' => 'Purworejo', 'university' => 'UKDW'],
            ['name' => 'Edwin', 'asal' => 'Yogyakarta', 'university' => 'UKDW'],
            ['name' => 'Kiki', 'asal' => 'Yogyakarta', 'university' => 'UKDW'],
            ['name' => 'Nuel', 'asal' => 'Tegal', 'university' => 'UKDW'],
            ['name' => 'Novianto', 'asal' => 'Sumatera selatang', 'university' => 'Atma'],
            ['name' => 'Bagas', 'asal' => 'Kalimantan', 'university' => 'Atma'],
            ['name' => 'Christo', 'asal' => 'Tegal', 'university' => 'Atma'],
            ['name' => 'Evan', 'asal' => 'Solo', 'university' => 'Atma'],
        ];

        DB::beginTransaction();

        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
