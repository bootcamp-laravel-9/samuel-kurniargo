@extends('layout/main')
@section('menu-profile', 'active')
@section('header-title', 'Profile')
@section('breadcrumb-title', 'Profile')
@section('content')
  <div class="container-fluid">
    <div class="section" id="basic-information">
      <div class="container">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img src="{{asset('AdminLTE/dist/img/user2-160x160.jpg')}}" class=" w-100" alt="User Image">
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="card-body">
                <div class="h4 mt-0 title">Basic Information</div>
                <div class="row">
                  <div class="col-sm-4"><strong class="text-uppercase">Age:</strong></div>
                  <div class="col-sm-8">24</div>
                </div>
                <div class="row mt-3">
                  <div class="col-sm-4"><strong class="text-uppercase">Email:</strong></div>
                  <div class="col-sm-8">anthony@company.com</div>
                </div>
                <div class="row mt-3">
                  <div class="col-sm-4"><strong class="text-uppercase">Phone:</strong></div>
                  <div class="col-sm-8">+1718-111-0011</div>
                </div>
                <div class="row mt-3">
                  <div class="col-sm-4"><strong class="text-uppercase">Address:</strong></div>
                  <div class="col-sm-8">140, City Center, New York, U.S.A</div>
                </div>
                <div class="row mt-3">
                  <div class="col-sm-4"><strong class="text-uppercase">Language:</strong></div>
                  <div class="col-sm-8">English, German, French</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="about">
      <div class="container">
        <div class="card">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card-body">
                <div class="h4 mt-0 title">About</div>
                <p>Hello! I am Anthony Barnett. Web Developer, Graphic Designer and Photographer.</p>
                <p>Creative CV is a HTML resume template for professionals. Built with Bootstrap 4, Now UI Kit and
                  FontAwesome, this modern and responsive design template is perfect to showcase your portfolio,
                  skills and experience. <a href="https://templateflip.com/templates/creative-cv/" target="_blank">Learn
                    More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="education">
      <div class="container cc-education">
        <div class="h4 text-center mb-4 title">Education</div>
        <div class="card">
          <div class="row">
            <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body cc-education-header">
                <p>2013 - 2015</p>
                <div class="h5">Master's Degree</div>
              </div>
            </div>
            <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body">
                <div class="h5">Master of Information Technology</div>
                <p class="category">University of Computer Science</p>
                <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis,
                  tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend
                  primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros
                  pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium
                  molestie ultricies sollicitudin dui.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="row">
            <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body cc-education-header">
                <p>2009 - 2013</p>
                <div class="h5">Bachelor's Degree</div>
              </div>
            </div>
            <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body">
                <div class="h5">Bachelor of Computer Science</div>
                <p class="category">University of Computer Science</p>
                <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis,
                  tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend
                  primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros
                  pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium
                  molestie ultricies sollicitudin dui.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="row">
            <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body cc-education-header">
                <p>2007 - 2009</p>
                <div class="h5">High School</div>
              </div>
            </div>
            <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body">
                <div class="h5">Science and Mathematics</div>
                <p class="category">School of Secondary board</p>
                <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis,
                  tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend
                  primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros
                  pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium
                  molestie ultricies sollicitudin dui.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="experience">
      <div class="container cc-experience">
        <div class="h4 text-center mb-4 title">Work Experience</div>
        <div class="card">
          <div class="row">
            <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body cc-experience-header">
                <p>March 2016 - Present</p>
                <div class="h5">CreativeM</div>
              </div>
            </div>
            <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body">
                <div class="h5">Front End Developer</div>
                <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis,
                  tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend
                  primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros
                  pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium
                  molestie ultricies sollicitudin dui.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="row">
            <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body cc-experience-header">
                <p>April 2014 - March 2016</p>
                <div class="h5">WebNote</div>
              </div>
            </div>
            <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body">
                <div class="h5">Web Developer</div>
                <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis,
                  tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend
                  primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros
                  pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium
                  molestie ultricies sollicitudin dui.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="row">
            <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body cc-experience-header">
                <p>April 2013 - February 2014</p>
                <div class="h5">WEBM</div>
              </div>
            </div>
            <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
              <div class="card-body">
                <div class="h5">Intern</div>
                <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis,
                  tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend
                  primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros
                  pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium
                  molestie ultricies sollicitudin dui.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="skill">
      <div class="container">
        <div class="h4 text-center mb-4 title">Professional Skills</div>
        <div class="card" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="progress-container progress-primary"><span class="progress-badge">HTML</span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary aos-init aos-animate" data-aos="progress-full" data-aos-offset="10"
                      data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                      aria-valuemax="100" style="width: 80%;"></div><span class="progress-value">80%</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="progress-container progress-primary"><span class="progress-badge">CSS</span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary aos-init aos-animate" data-aos="progress-full" data-aos-offset="10"
                      data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                      aria-valuemax="100" style="width: 75%;"></div><span class="progress-value">75%</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="progress-container progress-primary"><span class="progress-badge">JavaScript</span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary aos-init aos-animate" data-aos="progress-full" data-aos-offset="10"
                      data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                      aria-valuemax="100" style="width: 60%;"></div><span class="progress-value">60%</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="progress-container progress-primary"><span class="progress-badge">SASS</span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary aos-init aos-animate" data-aos="progress-full" data-aos-offset="10"
                      data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                      aria-valuemax="100" style="width: 60%;"></div><span class="progress-value">60%</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="progress-container progress-primary"><span class="progress-badge">Bootstrap</span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary aos-init aos-animate" data-aos="progress-full" data-aos-offset="10"
                      data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                      aria-valuemax="100" style="width: 75%;"></div><span class="progress-value">75%</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="progress-container progress-primary"><span class="progress-badge">Photoshop</span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary aos-init aos-animate" data-aos="progress-full" data-aos-offset="10"
                      data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                      aria-valuemax="100" style="width: 70%;"></div><span class="progress-value">70%</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
@endsection
