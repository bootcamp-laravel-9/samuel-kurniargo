@extends('layout/main')
@section('menu-table', 'active')
@section('header-title', 'Bootcamp list')
@section('breadcrumb-title', 'Bootcamp list')
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bootcamp Batch 9</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama</th>
                  <th>Asal kampus</th>
                  <th>Asal daerah</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row['name'] }}</td>
                    @if ($row['university'] == 'Atma')
                      <td style="background-color: green">Atma</td>
                    @elseif($row['university'] == 'UKDW')
                      <td style="background-color: blue">UKDW</td>
                    @else
                      <td style="background-color: red">Upn</td>
                    @endif
                    <td>{{ $row['asal'] }}</td>
                    <td>
                      <a href="{{url('user/member-detail/'.$row->id)}}" class="btn btn-primary">Detail</a>
                      <a href="{{url('user/member-edit/'.$row->id)}}" class="btn btn-success">Edit</a>
                      <a href="{{url('user/member-delete/'.$row->id)}}" class="btn btn-danger">Delete</a>
                    </td>       
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
              <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>
          </div>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    @endsection
