@extends('layout/main')
@section('menu-table', 'active')
@section('header-title', 'Bootcamp list detail')
@section('breadcrumb-title', 'Bootcamp list')
@section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Detail Member</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{url('edit-member-edit')}}" method="POST">
        <input type="hidden" name="id" value="{{$member->id}}">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$member->name}}">
        </div>
        <div class="form-group">
          <label for="university">University</label>
          <input type="text" class="form-control" id="university" name="university" value="{{$member->university}}">
        </div>
        <div class="form-group">
          <label for="asal">Asal</label>
          <input type="text" class="form-control" id="asal" name="asal" value="{{$member->asal}}" >
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/user" class="btn btn-danger">Batal</a>
      </div>
    </form>
  </div>
@endsection
