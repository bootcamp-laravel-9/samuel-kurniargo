<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'AuthController@login');
Route::get('register', 'AuthController@register');

Route::get('/', function () {
    return view('dashboard.index');
});

Route::get('/user', [UserController::class, 'index']);
Route::get('/user/member-detail/{id}', 'UserController@show');
Route::get('/user/member-edit/{id}', 'UserController@edit');
Route::post('/user/edit-member-proses', 'UserController@update');
Route::get('/user/member-delete/{id}', 'UserController@destroy');

// Route::get('/profile', [ProfileController::class, 'index']);
Route::get('/profile', 'ProfileController@index');

// Route::resource('user', UserController::class);

Route::get('/belajar', function () {
    return view('belajar-blade');
});
